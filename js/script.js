$(document).ready(function () {
    $('.button__arrowDown').click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $('.preHeading__secondary').offset().top
        }, 500);
    });


    $('.button__hamburger').click(function () {
        $('.button__hamburger').html('<i class="fas fa-times"></i>');
        $('.mobileMenu__navigation').toggleClass('mobileMenu__navigation-visible').slideToggle(500, function () {
            if ($(this).css('display') === 'none') {
                $('.button__hamburger').html('<i class="fas fa-bars"></i>');
                $(this).removeAttr('style');

            }
        });

    });


    var $window = $(window),
        $mMenu = $('.mobileMenu__navigation');
    $window.resize(function resize() {
        if ($window.width() > 960) {
            $mMenu.removeClass('mobileMenu__navigation-visible').removeAttr('style');
            $('.button__hamburger').html('<i class="fas fa-bars"></i>');
        }
    }).trigger('resize');
});